package com.transaction.transaction.service.impl;

import com.transaction.transaction.exception.HTTP404Exception;
import com.transaction.transaction.model.Transaction;
import com.transaction.transaction.model.dao.TransactionInput;
import com.transaction.transaction.model.dao.TransactionListResponse;
import com.transaction.transaction.repository.TransactionRepository;
import com.transaction.transaction.service.TransactionService;
import com.transaction.transaction.util.JsonUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public void createTransaction(TransactionInput transactionInput){


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

        LocalDate localDate = LocalDate.parse(transactionInput.getDate(), formatter);

        Transaction transaction = new Transaction();
        transaction.setId(transactionInput.getId());
        transaction.setDate(localDate);
        transaction.setCredit(transaction.getCredit());
        transaction.setDebit(transaction.getDebit());
        transaction.setType(transactionInput.getType());
        transaction.setDescription(transactionInput.getDescription());
        transaction.setBalance(transactionInput.getBalance());

        transactionRepository.save(transaction);

    }

    public void addMultipleTransactions(List<TransactionInput> tnxs){
        for(TransactionInput transactionInput : tnxs){
            this.createTransaction(transactionInput);
        }
    }

    public TransactionListResponse getAll(int page, int size){

        try{

            Pageable paging = PageRequest.of(page, size);
            Page<Transaction> pageTnx = transactionRepository.findAll(paging);

            TransactionListResponse transactionListResponse = new TransactionListResponse();
            transactionListResponse.setTransactionList(pageTnx.getContent());
            transactionListResponse.setCurrentPage(pageTnx.getNumber());
            transactionListResponse.setTotalPages(pageTnx.getTotalPages());
            transactionListResponse.setTotalItems(pageTnx.getTotalElements());

            return transactionListResponse;

        }catch (Exception e){
            throw new HTTP404Exception("Data not available");

        }

    }
}
