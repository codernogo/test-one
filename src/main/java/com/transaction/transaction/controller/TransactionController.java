package com.transaction.transaction.controller;

import com.transaction.transaction.model.dao.TransactionInput;
import com.transaction.transaction.service.TransactionService;
import com.transaction.transaction.util.ResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class TransactionController {

    @Autowired
    private final TransactionService transactionService;


    @PostMapping("/v1/transactions")
    public ResponseEntity<?> addTransaction(@Validated @RequestBody TransactionInput transactionInput){
        transactionService.createTransaction(transactionInput);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseModel<>("00","Transaction added successfully","SUCCESS"));
    }

    @PostMapping("/v1/transaction-list")
    public ResponseEntity<?> addMultipleTransactions(@Validated @RequestBody List<TransactionInput> transactionInput){
        transactionService.addMultipleTransactions(transactionInput);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseModel<>("00","Multiple Transactions added successfully","SUCCESS"));
    }

    @GetMapping("/v1/transactions")
    public ResponseEntity<?> getAllTransactions(@RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "10") int size){
        return ResponseEntity.ok(new ResponseModel<>("00","Success",transactionService.getAll(page,size)));
    }


}
