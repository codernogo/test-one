package com.transaction.transaction.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "TRANSACTION")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Transaction {

    @Id
    @GeneratedValue(generator  = "custom")
    @GenericGenerator(name = "custom", strategy = "uuid")
    @Column(name = "ID",length = 100)
    private String id;

    @Column(name = "DATE",length = 50)
    private LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE", length = 30)
    private Type type;

    @Column(name = "DESCRIPTION", length = 100)
    private String description;

    @Column(name = "DEBIT")
    private double debit;

    @Column(name = "CREDIT")
    private double credit;

    @Column(name = "BALANCE", length = 100)
    private String balance;
}
