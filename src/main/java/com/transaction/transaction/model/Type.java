package com.transaction.transaction.model;

public enum Type {

    FPI,
    CPT,
    DDX,
    DEB,
    TFR,
    FPO
}
