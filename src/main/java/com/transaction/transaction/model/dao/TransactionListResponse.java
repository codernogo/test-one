package com.transaction.transaction.model.dao;

import com.transaction.transaction.model.Transaction;
import lombok.Data;

import java.util.List;

@Data
public class TransactionListResponse {

    private List<Transaction> transactionList;
    private int currentPage;
    private Long totalItems;
    private int totalPages;

}

